#include <ros/ros.h>
#include <unistd.h>
#include <vector>
#include <std_msgs/Header.h>
#include <std_msgs/String.h>
#include "boost/date_time/posix_time/posix_time.hpp"

#define QUEUE_SZ_LIMIT 3
#define QUEUE_DISPLAY_TIME_MS 1000

std::vector<std_msgs::Header> string_queue;

//using sequence as source id #
void string2fifocallback(std_msgs::Header m)
{
  //stamp the incoming message with the current time
  m.stamp=ros::Time::now();

  //if our queue is empty push the message to it
  if(string_queue.size()<1)
  {
    string_queue.push_back(m);
    //ROS_INFO("Adding string %s",m.frame_id.c_str());
  }
  else
  {
    int matching_src_index = -1;
    int oldest_time_index = -1;
    ros::Time oldest_time=ros::Time::now();
    //loop through all the messages
    for(int i=0; i<string_queue.size();i++)
    {
      //if the messages are from the same source
      if(m.seq==string_queue.at(i).seq)
      {
        //ROS_INFO("Found matching source at index %d",i);
        matching_src_index = i;
      }

      //while we are in the loop grab the oldest time index
      if(m.stamp<oldest_time)
      {
        oldest_time = m.stamp;
        //ROS_INFO("Oldest timestamp at index %d",i);
        oldest_time_index = i;
      }
    }

    //if the message source id is already in the queue
    if(matching_src_index>-1)
    {
      //replace it.
      //ROS_INFO("Replaceing matching source at index %d",matching_src_index);
      string_queue.at(matching_src_index)=m;
    }
    else
    {
      if(string_queue.size()>=QUEUE_SZ_LIMIT)
      {
        //remove oldest message and put in new one;
        //ROS_INFO("Replacing oldest string at %d",oldest_time_index);
        string_queue.at(oldest_time_index)=m;
      }
      else
      {
        //its a new string, but our queue isnt full.  
        string_queue.push_back(m);
      }
    }
  }
}

int main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "stringserver");
  ros::NodeHandle n;
  ros::Rate loop_rate(10);

  ros::Subscriber sub;
  ros::Publisher pub;

  sub = n.subscribe("stringtofifo",1,string2fifocallback);
  pub = n.advertise<std_msgs::String>("fifotodisplay",1);

  uint32_t itr=0;

  std_msgs::Header clock_msg;

  ros::Time timecheck=ros::Time::now();

  while(ros::ok())
  {
    //if it has been a second update the clock
    // if(1)
    // {
    //   clock_msg.seq=getpid();
    //   clock_msg.stamp = ros::Time::now();
    //   std::string msg;
    //   boost::posix_time::ptime my_posix_time = ros::Time::now().toBoost();
    //   std::string iso_time_str = boost::posix_time::to_iso_extended_string(my_posix_time);
    //   clock_msg.frame_id = "Its around ";// + iso_time_str;
    //   ROS_INFO("%s",clock_msg.frame_id.c_str());
    // }

    //if its time to publish a new string
    if(timecheck<ros::Time::now())
    {
      //new time is some time in the future
      timecheck=ros::Time::now()+ros::Duration(2);

      //make sure the string isnt empty
      if(string_queue.size()>0)
      { 
        //and the iterator is not pointing past the end of the queue
        if(itr<=string_queue.size()-1)
        {
          //publish the string
          std_msgs::String s;
          s.data = string_queue.at(itr).frame_id.c_str();
          pub.publish(s);
          //inc iterator, make sure it isnt past the limits
          itr++;
          if(itr>=QUEUE_SZ_LIMIT)itr = 0;
          if(itr>=string_queue.size())itr = 0;
        }
      }
    }
    loop_rate.sleep();
    ros::spinOnce();
  }
  return 0;
}
